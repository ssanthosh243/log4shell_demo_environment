#!/bin/bash

if [[ $ENABLE_ELASTIC -eq "True" ]]
then
    # Enrolling the client
    /opt/elastic-agent-7.15.2-linux-x86_64/elastic-agent enroll -f --url=$ES_URL --enrollment-token=$ENROLLMENT_TOKEN

    # Running the elastic Agent in background
    /opt/elastic-agent-7.15.2-linux-x86_64/elastic-agent run > /dev/null &
else
    echo "Elastic Not Configured"
fi

# Running the vulnerable version
/usr/bin/java -jar /app/log4shell-vulnerable-app-0.0.1-SNAPSHOT.jar 