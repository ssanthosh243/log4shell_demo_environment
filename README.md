# Log4Shell Demo environment

**This content is for educational and research purposes only. Do not attempt to violate the law with anything contained here !!!**

To run this environment you will need docker and docker-compose

1. Clone this repository
2. Rename the file `elastic-example.env` to `elastic.env`
3. If you intend to use it with Elastic, Set the value `ENABLE_ELASTIC` to `True` in `elastic.env` file along with the details from Elastic Fleet
4. Run the command `docker-compose up -d`
5. To run the attack, access the shell of attacker container using the command `docker exec -it log4shell-attacker /bin/sh`

## To-Do
- [ ] Run the vulnerable web application using web server
- [ ] Build a better attacker machine
- [ ] Add support for Velociraptor on vulnerable web app container

### Credits
1. [log4shell-vulnerable-app](https://github.com/christophetd/log4shell-vulnerable-app)
2. [JNDIExploit](https://github.com/feihong-cs/JNDIExploit)
